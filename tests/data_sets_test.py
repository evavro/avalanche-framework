from lib.block import Block
from blocks.data.sets import *


def test_unique():
    assert Unique.s([1, 2, 3, 4, 4, 5, 5, 5, 5])().get() == [1, 2, 3, 4, 5]
    assert Unique.s(['dog', 'cat', 'bird', 'dog'])().get() == ['bird', 'dog', 'cat']

def test_union():
    assert Union.s([1, 2, 3, 4, 5], [3, 4, 5, 6])().get() == [1, 2, 3, 4, 5, 6]
    assert Union.s(['dog', 'cat'], ['cat', 'pig', 'bat'])().get()  == ['bat', 'pig', 'dog', 'cat']

def test_intersection():
    assert Intersection.s([1, 2, 3, 4, 5], [3, 4, 6, 7])().get() == [3, 4]
    assert Intersection.s(['dog', 'cat', 'mouse'], ['cat'])().get() == ['cat']

def test_difference():
    assert Difference.s([1, 2, 3, 4, 5, 7], [2, 3, 4, 5, 6])().get() == [1, 7]
    assert Difference.s(['dog', 'cat', 'mouse'], ['cat', 'mouse'])().get() == ['dog']
