__author__ = 'Eric'

import sys

sys.path.append("C:/Users/Eric/projects/av-framework/src")

from lib.block import Block
from blocks.workflow import *
from celery import task, Celery
import celeryconfig

from blocks.workflow import Scheduler
import networkx as nx

wfj = {'workflows': {
    'connections': {'b0': {'outputs': {'0': 'b1'}, 'inputs': {'0': None}}, 'b1': {'outputs': {'0': None}, 'inputs': {'0': 'b0'}}},
    'blocks': {'b0': {'params': {'message': 'Hello'}, 'class': 'MessageProxy'},
               'b1': {'params': {}, 'class': 'MessageProxy'}}}}

wf2 = {
    'workflows': {
        'blocks': {
            'b0': {
                'class': 'RandomNumberSource',
                'params': {}
            },
            'b1': {
                'class': 'RandomNumberSource',
                'params': {}
            },
            'b2': {
                'class': 'Union',
                'params': {}
            },
            'b3': {
                'class': 'Sort',
                'params': {}
            },
            'b4': {
                'class': 'Intersection',
                'params': {}
            },
            'b5': {
                'class': 'Printer',
                'params': {}
            }
        },
        'connections': {
            'b0': {
                'inputs': {
                    0: None
                },
                'outputs': {
                    0: ['b2', 'b3', 'b4']
                }
            },
            'b1': {

                'inputs': {
                    0: None
                },
                'outputs': {
                    0: ['b2', 'b4']
                }
            },
            'b2': {
                'inputs': {
                    0: 'b0',
                    1: 'b1'
                },
                'outputs': {
                    0: ['b5']
                }
            },
            'b3': {
                'inputs': {
                    '0': 'b0'
                },
                'outputs': {
                    '0': ['b5']
                }
            },
            'b4': {
                'inputs': {
                    '0': 'b0',
                    '1': 'b1'
                },
                'outputs': {
                    '0': ['b5']
                }
            },
            'b5': {
                'inputs': {
                    '0': 'b2',
                    '1': 'b3',
                    '2': 'b4'
                },
                'outputs': {
                    '0': None
                }
            }
        }
    }
}

r1_r = WorkflowParser.delay(wf2)

wfs = r1_r.get()

for wf in wfs:
    for rv in sorted(wf):
        print(rv, wf[rv], wf[rv].get())