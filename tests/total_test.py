from celery import task
from lib.block import Block
import celery

# FIXME - This needs to be moved to somewhere within the 'blocks.' package in order for celery to recognize it
@task(base=Block)
class Total(Block):

	meta = {
		'name': 'Total of given values',
		'desc': 'Discovers total of given numbers',
		'connections': {
			'inputs': {
				'0': {
					'name': 'inputs',
					'desc': 'Data to be printed',
					'type': object
				}
			},
			'outputs': {
				'0': {
					'name': 'total_val',
					'desc': 'The total value of the data',
					'type': float
				}
			}
		},
		"params": {}
   }
	
def run(self, *input, **params):
	for i in input:
		totalsum += i
	return totalsum
	
def test_case1():
	arraytest = [5,4,6,4,5]
	diction = {'field':0}
	#assert Total.s(arraytest,diction)().get() == 24
