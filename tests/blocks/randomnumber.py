from celery import task
from avalanche.blocks import Block
import celery
from random import randint

@task(base=Block)
class RandomNumber(Block):

	meta = {
		'name': 'RandomNumber',
		'desc': 'Gives a random number between two user params',
		'connections': {
			'inputs': {
				'0': {
					'name': 'inputs',
					'desc': 'Data to be printed',
					'type': object
				}
			},
			'outputs': {
				'0': {
					'name': 'rand_val',
					'desc': 'Random Value between two values',
					'type': int
				}
			}
		},
		"params": {
			'lower': {
				'type': int,
				'default': '0',
				'desc': "The lower range of the random number"
			},
			'upper': {
				'type': int,
				'default': '10',
				'desc': "The upper range of the random number"
			}
		}
   }
   
	def run(self, *input, **params):
		lower = params['lower']
		upper = params['upper']
		randNumber = randint(lower, upper)
		return randNumber
	
def test_case1(self):
	arraytest = array([5,4,6,4,5])
	diction = {'lower':1, 'upper':10}
	self.run(arraytest,diction)

