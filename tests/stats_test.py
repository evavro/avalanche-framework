from blocks.stats import *

def test_max():
    res = Max.delay([5,4,6,4,5]).get()
    assert res == 6
    
def test_mean():
    res = Mean.delay([1,2,3,4,5,6]).get()
    assert res == 3.5
    
def test_median():
    res = Median.delay([5,4,6,4,5]).get()
    assert res == 5.0
    
def test_min():
    res = Min.delay([5,4,6,4,5]).get()
    assert res == 4
    
def test_std_deviation():
    pass