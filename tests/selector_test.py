from lib.selector import val_list
import numpy.random as nprd

def test_val_list():
    l1_m = val_list([[1, 1], [1, 1]])
    l2_m = val_list([[[1, 2], [3, 4]]])

    assert l1_m == [1, 1, 1, 1]
    assert l2_m == [1, 2, 3, 4]
        
def test_val_list_type_filter():
    assert val_list(['dog', 'cat', 'fish', 0], type_filter=int) == [0]
    assert val_list(['dog', 'cat', 'fish'], type_filter=int) == []
    
def test_val_list_key_filter():
    d1_m = val_list({'people':{'0':{'name': 'Sam','age': 22},'1':{'name': 'Tom','age': 34},'2': {'name': 'Erin', 'age': 24}}}, key_filter='name')
    d2_m = val_list({'nests': {'nest1': {'eggs': {'egg1': 'e1','egg2': 'e2'}}}, 'nest2': {'eggs': {'egg3': 'e3','egg4': 'e4'}}}, key_filter='eggs')
    d3_m = val_list({'nothing': {'inputs': 'here'}}, key_filter='bad_key')
    d4_m = val_list([0, 1, 2, 3], key_filter='key_on_non_dictionary')
    
    for name in d1_m:
        assert name in ['Tom', 'Sam', 'Erin']
    for egg in d2_m:
        assert egg in [{'egg1': 'e1','egg2': 'e2'},{'egg3': 'e3','egg2': 'e4'}]
    assert d2_m == []
    assert d4_m == [0, 1, 2, 3]
    
def test_val_list_both_filters():
    d1_m = val_list({'people':{'0':{'name': 'Sam','age': 'INVALID'},'1':{'name': 'Tom','age': 34},'2': {'name': 'Erin', 'age': 'INVALID'}}}, key_filter='age', type_filter=int)
    assert d1_m == [34]
    
# if the data is anything but a list or dictionary, return it back (this will naturally work with numpy)
def test_val_list_numpy_data():
    data = nprd.random_integers(0, 100, 250)
    # FIXME numpy whines, but the values are as expected
    #assert val_list(data) == data.all()
    
def test_flatten():
    pass