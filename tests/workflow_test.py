__author__ = 'Erik'

from lib.block import Block
from blocks.workflow import Workflow, WorkflowParser
from celery import task, Celery
import celeryconfig

#celery = Celery()
#celery.config_from_object(celeryconfig)

wfp = WorkflowParser

def test_json_parse():
    wfp = WorkflowParser
    message = "Great success"
    result = WorkflowParser.run({"workflows": {
                        "blocks": {
                            "b0": {
                                   "class": "MessageProxy",
                                   "params":{
                                            "message": message
                                    }
                            },
                            "b1": {
                                   "class": "MessageProxy",
                                   "params":{}
                            }
                        },
                        "connections":{
                            "b0":{
                                    "inputs":{
                                         "0": None
                                    },
                                    "outputs":{
                                         "0": "b1"
                                    }
                             },
                             "b1":{
                                   "inputs":{
                                         "0": "b0"
                                    },
                                   "outputs":{
                                          "0": None
                                    }
                            }
                        }
                    }
                })
    
    # DEPRECATED
    #assert result.get() == [[message]]

def get_start_blocks():
    pass

def missing_params():
    pass

def missing_connections():
    pass


test_json_parse()