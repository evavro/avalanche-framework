from celery import Celery, task, chain, group
from celery.utils.log import get_task_logger
from nose.tools import *
import celeryconfig

from lib.block import Block, result_history
from lib.db import MongoDB
from blocks.misc.testing import *
    
celery = Celery()
celery.config_from_object(celeryconfig)

logger = get_task_logger(__name__)


def test_group():
    res = group(AddBlock.s(2, 3), AddBlock.s(5, 0))().get()
    assert res == [5, 5]

def test_chain():
    res = (MultiplyBlock.s(2, 3) | MultiplyBlock.s(5))().get()
    assert res == 30
    
def test_manual_chain():
    
    def build_chain():
        r = [AddBlock.s(0, 0)]
        for i in range(4):
            r.append(AddBlock.s(i))
        return r
    
    chain1 = chain(build_chain())
    chain2 = (AddBlock.s(0, 0) | AddBlock.s(1) | AddBlock.s(2) | AddBlock.s(3))
    
    print('Chain 1: %s' % (chain1().get(),))
    print('Chain 2: %s' % (chain2().get(),))
    
    assert chain1().get() == chain2().get() == 6
    
def test_heavy_chain():
    res = (RandomNumberSource.s(**{'min': 0, 'max': 10, 'num': 2500}) | Printer.s())().get()
    assert len(res[0]) == 2500
    
def test_store_results():
    db = MongoDB()
    res = StoredAddition.s(**{'x': 1, 'y': 2})().get()
    assert res == 3, "Result: %s != %s" % (res, 3)
    
    res_hist = result_history('StoredAddition')
    
    assert len(res_hist) > 0
    
    for obj in res_hist:
        assert obj == res
        
    # clean up results once it has a big enough population
    if len(res_hist) > 5:
        db.remove('StoredAddition')
        
def test_strict_input():
    res = StrictAddition.s(**{'x': 1, 'y': 2})().get()
    assert res == 3
    
@raises(KeyError)
def test_strict_input_invalid_key():    
    res = StrictAddition.s(**{'x': 1, 'z': 2})().get()
    
@raises(TypeError)
def test_strict_input_invalid_type():    
    res = StrictAddition.s(**{'x': 1, 'y': 'cat'})().get()
    
def test_detect_result_safe_input():
    params = {'task1': RandomNumberSource.s(), 'task2': AddBlock.s(2,3)}
    kept_results = ResultInput.delay(None, 1, 'str', MultiplyBlock.s(3,3), **params).get()
    assert kept_results == False
    