'''
Created on Oct 20, 2012

@author: Jon
'''
from lib.block import Block
from blocks.mine.association_learning import AprioriAlgorithm

testdata1 = [(1), (1, 2), (2, 1), (1,3), (2, 3), (2), (2),(1), (1, 2, 3), (1, 2, 3, 4, 5)]
testdata2 = [('milk'), ('eggs'), ('milk', 'eggs'), ('eggs', 'milk'), ('milk', 'eggs', 'bread'), ('bread', 'eggs'), ('eggs', 'milk', 'bread', 'beer'),('milk', 'bread', 'beer')]
processor = AprioriAlgorithm(2, 2)
processor.run(testdata2)
print "longest set in data", processor.longest_set
print "items:", processor.items
count = 1
for x in processor.frequent_sets:
    c_set = x.getCandidateSets()
    f_set = x.getFrequentSets()
    print "candidate sets for frequent item set", count
    for s in c_set:
        print s, ":", c_set[s]

    print "frequent sets for frequent item set", count
    for f in f_set:
        print f, ":", f_set[f]
    count +=1
print "finished test"
