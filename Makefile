init:
	pip install -r requirements.txt

celery:
	celeryd -a avalanche worker --loglevel=info -

run:
	gunicorn -c gunicorn.conf -b 127.0.0.1:5000 app:app --debug

test:
	nosetests -v tests/* src/*

doc: 
	make -C docs html
