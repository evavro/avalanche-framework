from celery import task
from lib.block import Block, ignore

'''
The task of generalizing a known structure to apply to new data.
For example, an e-mail program might attempt to classify an e-mail as "legitimate" or as "spam".
'''
@ignore
@task(base=Block)
class ClassificationBlock:
    
    # http://scikit-learn.org/0.11/tutorial/statistical_inference/supervised_learning.html#k-nearest-neighbors-classifier

    def __init__(self):
        print 'TODO'
        
    def run(self, *args, **params):
        data = self.input('data', args, params)

        '''
        Incoming data = New Observation
        Training set = Old data
        Goal: find in the training set (i.e. the data used to train the estimator) the observation with the closest feature vector
        '''