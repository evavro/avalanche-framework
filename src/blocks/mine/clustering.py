from celery import task
from lib.block import Block, ignore_meta
from lib.selector import sv_matrix
from sklearn import cluster
import numpy as np


@ignore_meta
@task(base=Block)
class KMeansCluster(Block):
    
    meta = {
        'name': 'K-Means Clustering',
        'desc': 'Calculates the k-means cluster values in a set of data',
        'connections': {
            'inputs': {
                '0': {
                    'name': 'data',
                    'desc': 'The incoming data set',
                    'type': list
                }
            },
            'outputs': {
                '0': {
                    'name': 'cluster_map',
                    'desc': 'The original data set mapped to cluster values'
                },
                '1': {
                    'name': 'cluster_centers',
                    'desc': 'The center values for each cluster'
                }
            }
        },
        'params': {
            'field': {
                'type': str,
                'default': '',
                'desc': 'The field of the dataset to find k-cluster values from'
            },
            'n_clusters': {
                'type': int,
                'default': 3,
                'desc': 'The number of clusters to split the data into'
            }
        }
    }
     
    def run(self, data, field=None, n_clusters=3):
        data = sv_matrix(data, key_filter=field)
        k_means = cluster.KMeans(n_clusters=n_clusters)
        k_means.fit(data)
        k_labels = k_means.labels_
        cluster_centers = k_means.cluster_centers_.squeeze()
        cluster_vals = np.choose(k_labels, cluster_centers)
        
        cluster_map = dict([(i, []) for i in k_labels])
        
        for (i, kl) in enumerate(k_labels):
            cluster_map[kl].append(data[i][0])
            
        return cluster_map
        