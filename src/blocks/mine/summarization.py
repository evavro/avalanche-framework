__author__ = 'Erik'

from celery import task
from lib.block import Block, ignore


'''
Provides a more compact/specific representation of the data set, including visualization and report generation.
'''
@ignore
@task(base=Block)
class SummarizationBlock:

    def __init__(self):
        super(self)
        