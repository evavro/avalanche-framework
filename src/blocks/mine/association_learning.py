__author__ = 'Jon'
import itertools
from celery import task
from lib.block import Block
import json


class FrequentItemSets:
    item_set = dict()
    candidate_set = dict()
    frequent_set = dict()
    relations = dict()
    set_length = 0

    def __init__(self, length):
        self.set_length = length
        self.item_set = dict()
        self.candidate_set = dict()
        self.frequent_set = dict()
        self.relations = dict()

    def addSet(self, set_added, frequency):
        self.frequent_set.update({set_added : frequency})

    def addCandidate(self, set_added, frequency):
        self.candidate_set.update({set_added: frequency})

    def getCandidateSets(self):
        return self.candidate_set

    def genFrequentSets(self, min_supp):
        for x in self.candidate_set:
            if x.value >= min_supp:
                self.addSet(x, x.value)
    def getFrequentSets(self):
        return self.frequent_set

    def addRelation(self, original, implied_set):
        self.relations.update({original:implied_set})
    def getRelations(self):
        return self.relations

'''
Reads in the data and finds relations between the items in the data using the
Apriori Algorithm
'''
#@task(base=Block)
class AprioriAlgorithm():

    #read in the data to be processed
    def readData(self, variables):
        json_data = json.load(self.json_file)
        self_data = variables['data']
        self_items = variables['items']
        self_transactions = variables['transactions']
        self_longest_set = variables['longest_set']
        for sets in json_data:
            tup = tuple()
            data = json_data[sets]
            parts = data.split(',')
            for x in parts: #create a tuple out of the data so that it is easier to handle
                x = x.strip()
                tup = tup + (x,)
            self_data.append(tup)
            for item in tup:
                if item not in self_items:
                    self_items.append(item)
                if len(tup) > self_longest_set:
                    self_longest_set = len(tup)
            self_transactions += 1
        variables['data']=self_data
        variables['items']=self_items
        variables['longest_set']=self_longest_set
        variables['transactions']=self_transactions

    #first two frequency sets do not require the pruning of the sets
    def firstFrequencySet(self, variables):
        self_num_frequent_sets = variables['num_frequent_sets']
        self_data = variables['data']
        self_items = variables['items']
        self_frequent_sets = variables['frequent_sets']
        self_num_frequent_sets += 1
        frequent_set = FrequentItemSets(self_num_frequent_sets)
        for item in self_items:
            frequency = 0
            for sets in self_data:
                #if sets is a tuple then ckeck if item is in sets otherwise compare sets to item
                if type(sets) is tuple:
                    if item in sets:
                        frequency += 1
                elif item == sets:
                    frequency += 1
            if frequency > 0:
                frequent_set.addCandidate(item, frequency)
        self_frequent_sets.append(frequent_set)
        variables['num_frequent_sets'] = self_num_frequent_sets
        self.processCandidates(1, variables)
        self.joinSets(1, variables)
        self.processCandidates(2, variables); #process the candidates of 2-item sets
    #find candidates for the next frequency set by joining the current frequency sets

    
    def setNotInCandidates(self, joined_set, candidate_set):
        for x in candidate_set.keys():
            if set(x) == set(joined_set):
                return False
        return True
    
    
    def joinSets(self, length, variables):
        self_frequent_sets = variables['frequent_sets']
        fset = self_frequent_sets[length -1]
        joining_sets = fset.getFrequentSets()
        new_fset = FrequentItemSets(length + 1) #create a new frequencyItemSet of with sets of length length+1
        set_frequency = 0
        joined_set = None
        for sets in joining_sets:
            for rest_of_sets in joining_sets:
                #don't compare the same 2 sets, want all but one element to match the other set
                if sets != rest_of_sets and self.compareSets(sets, rest_of_sets, length - 1):
                #join sets together and add it to the set of candidates
                    if(length == 1): #we can just create a tuple of the two sets):
                        joined_set = tuple((sets, rest_of_sets))
                    else:
                        joined_set = set(sets).union(set(rest_of_sets)) #convert to set to perform a union on the sets
                        joined_set = tuple(joined_set) #convert back to tuple
                    #get the frequency set of the current set
                    set_frequency = self.getSetFrequency(joined_set, variables)
                    #add set to the other candidate sets for length if it doesn't exist already
                    if len(new_fset.candidate_set)==0 or self.setNotInCandidates(joined_set, new_fset.candidate_set):
                        if(length > 1):#for all joinSets operations greater than length 2 we want to prune the sets
                            if(self.pruneSets(joined_set, length, variables)):#length - 1 because we are comparing it to the previous frequent set
                                new_fset.addCandidate(joined_set, set_frequency)
                        else:
                            new_fset.addCandidate(joined_set, set_frequency)
        self_frequent_sets.append(new_fset)

    # compares two sets together, if there are enough matching elements between the two sets True
    # else False
    def compareSets(self, set_one, set_two, matching):
        matches = 0
        if(matching == 0):
            return True
        else:
            for item in set_one:
                if item in set_two:
                    matches += 1
        if matches >= matching:
            return True
        else:
            return False

    #prunes the set of candidates to prevent them from getting too large
    def pruneSets(self, c_set, length, variables):
        subsets = []
        self_frequent_sets = variables['frequent_sets']
        tuples = list(itertools.combinations(c_set, length))
        fset = self_frequent_sets[length -1]
        for t in tuples:
            prune = False
            #if each subset is part of the frequent set of length-itemsets then it remains
            #otherwise it is removed from the list
            for c in fset.getFrequentSets().keys():
                if set(t) == set(c):
                    prune = True
            if(not prune):
                return prune #if it is not in the previous frequent set prune this candidate set
        return prune


    #gets the frequency of the set
    def getSetFrequency(self, curr_set, variables):
            count = 0
            self_data = variables['data']
            for sets in self_data:
                #if sets is not a tuple then it will not contain curr_set
                if (type(sets) is tuple) and (set(curr_set).issubset(set(sets)) or curr_set == sets):
                    count += 1
            return count

    #determine if candidates of a set meet the min_supp and add them to the frequency sets
    def processCandidates(self, length, variables):
        self_frequent_sets = variables['frequent_sets']
        print len(self_frequent_sets)
        self_min_supp = variables['min_supp']
        c_set = self_frequent_sets[length - 1].getCandidateSets()
        for key in c_set.keys():
            if c_set[key] >= self_min_supp:
                self_frequent_sets[length - 1].addSet(key, c_set[key])

    #find the relations between items
    def getRelations(self, variables):
        self_frequent_sets = variables['frequent_sets']
        self_min_conf = variables['min_conf']
        for fsets in self_frequent_sets:
            if fsets.set_length != 1:
                for fset in fsets.getFrequentSets():
                    length = 1
                    subsets = []
                    while length < len(fset):
                        s = (list(itertools.combinations(fset, length)))
                        for x in s:
                            subsets.append(x)
                        length += 1
                    for x in subsets:
                        confidence = fsets.frequent_set[fset]/float(self.getSetFrequency(x, variables)) * 100
                        if confidence >= self_min_conf:
                            fsets.addRelation(x, tuple(set(fset).difference(set(x))))

    def run(self, input, **params):
        data=[]
        frequent_sets=[]
        items = []
        transactions = 0
        min_supp = params['min_supp']
        min_conf = params['min_conf']
        longest_set = 0
        num_frequent_sets = 0
        variables = {'data':data, 'frequent_sets':frequent_sets, 'items':items, 'transactions':transactions, 'min_supp':min_supp, 'min_conf':min_conf,'longest_set':longest_set, 'num_frequent_sets':num_frequent_sets}
        self.json_file = open(input)
        self.readData(variables)
        self_longest_set = variables['longest_set']
        self_frequent_sets = variables['frequent_sets']
        self.firstFrequencySet(variables)
        for x in variables:
            print x, variables[x]
        length = 2
        print self_longest_set
        while length < self_longest_set:
            self.joinSets(length, variables)
            self.processCandidates(length + 1, variables)
            length += 1
        self.getRelations(variables)
        for x in self_frequent_sets:
            print x.frequent_set
        results = dict()
        for fset in self_frequent_sets:
            relation = fset.getRelations()
            for x in relation:
                results.update({x:relation[x]})
        for x in results:
            print x, '----->', results[x]
        return results


