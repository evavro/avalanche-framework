from celery import task
from sklearn import svm
from scipy.spatial import distance
from sklearn.cluster import DBSCAN
import numpy as np

from lib.block import Block, ignore
from lib.selector import val_list, sv_matrix
from lib.db import MongoDB


@ignore
@task(base=Block)
class Anomaly(Block):
    
    meta = {
        'name': "Anomaly Detection",
        'desc': "Calculates outlier and novelty items in a set of data",
        'connections': {
            'inputs': {
                '0': {
                    'name': 'new_data',
                    'desc': 'The incoming data set',
                    'type': list
                }
            },
            'outputs': {
                '0': {
                    'name': 'clean_data',
                    'desc': 'The original data set stripped of outliers'
                },
                '1': {
                    'name': 'outliers',
                    'desc': 'Outliers discovered in the data set'
                }
            }
        },
        'params': {
            'collection': {
                'type': str,
                'default': '',
                'desc': "The collection name used to contain outlier information"
            },
            'field': {
                'type': str,
                'default': '',
                'desc': "The field of the dataset to base outlier values off of"
            },
            'eps': {
                'type': float,
                'default': 0.95,
                'desc': "The maximum distance between two samples for them to be considered as in the same neighborhood"
            },
            'raw_data': {
                'type': bool,
                'default': False,
                'desc': "'True' if you are passing in raw, single variable data sets. 'False' if you are passing in JSON (dictionary)"
            }
        }
    }
    
    # http://scikit-learn.org/dev/modules/generated/sklearn.cluster.DBSCAN.html
    # http://scikit-learn.org/dev/auto_examples/cluster/plot_dbscan.html#example-cluster-plot-dbscan-py
    
    # Basic outlier detection without the use of a training set
    def outlier_detection(self, json_data, collection='outliers', outlier_field=None, eps=0.95, raw_data=False):
        if raw_data:
            field_vals = np.array(json_data)
        else:
            field_vals = sv_matrix(json_data, key_filter=outlier_field)
            
        # Compute similarities
        dist = distance.squareform(distance.pdist(field_vals))
        sims = 1 - (dist / np.max(dist))
        
        # Compute DBSCAN (Density-Based Spatial Clustering)
        db = DBSCAN(eps=eps, min_samples=10).fit(sims)
        labels = db.labels_
        
        # only support single variable data atm, hence [0]
        outlier_indexes = [i[0] for i in np.argwhere(labels == -1)]
        outlier_data = [field_vals[i][0] for i in outlier_indexes]
            
        # TODO - Figure out a way to link back to the original data set
        self.db.insert(collection, {'outliers': outlier_data}, meta_data={'field': outlier_field, 'task_id': self.task_id()})
        
        return outlier_data
        
            
    # http://scipy-lectures.github.com/advanced/scikit-learn/index.html#k-nearest-neighbors-classifier
    # http://scipy-lectures.github.com/advanced/scikit-learn/index.html
    
    # determines if the incoming data set should be considered an outlier or not - training set must be outlier free
    def novelty_detection(self, new_data, train_collection):
        old_data = self.db_data.query(collection=train_collection)
        
        # convert data to arrays supported by sklearn
        test_set  = val_list(new_data)
        train_set = val_list(old_data)
        outliers  = val_list([])
        
        clf = svm.OneClassSVM(nu=0.1, kernel="rbf", gamma=0.1)
        clf.fit(train_set)
        
        pred_train = clf.predict(train_set)
        pred_test = clf.predict(test_set)
        pred_outliers = clf.predict(outliers)
        
    
    def run(self, new_data, **params):
        self.db = MongoDB()
        
        return self.outlier_detection(new_data, collection = params.get('collection', self.db_name()),
                                                outlier_field = params.get('field', None),
                                                eps = params.get('eps', 0.95),
                                                raw_data = params.get('raw_data', False))
        

        