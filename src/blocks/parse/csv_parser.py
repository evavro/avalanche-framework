__author__ = 'Eric'

from lib.block import Block

from celery import task

import numpy

import csv


@task(base=Block)
class CSVParser(Block):

    meta = {
        'name': "CSV Parser",
        'desc': "Parses through data in the CSV format",
        'connections': {
            'inputs': {},
            'outputs': {
                '0':{
                    'type': list,
                    'name': 'csv_out',
                    'desc': 'File output'
                }
            }
        },
        'params': {
            #TODO Add parameters for dialect information, we should copy MS Excel CSV interface maybe
            'file':{
                'type': "file",
                'desc': "path to the csv file"
            }
        }
    }

    def run(self, file):
        with open(self.file) as csvfile:
            filereader = csv.reader(csvfile)
            for row in filereader:
                print row
                #TODO Feed the next block



