from celery import task
from lib.block import Block
from lib.selector import val_list
import scipy as sp

@task(base=Block)
class Median(Block):

	meta = {
		'name': 'Median',
		'desc': 'Discovers Median value in a field of a dataset',
		'connections': {
			'inputs': {
				'0': {
					'name': 'data',
					'desc': 'The source data set',
					'type': list
				}
			},
			'outputs': {
				'0': {
					'name': 'median_value',
					'desc': 'The max value of the data',
					'type': int
				}
			}
		},
		'params': {
			'field': {
				'type': str,
				'default': '',
				'desc': "The field of the data set to base the median value off of"
			}
		}
   	}

	def run(self, data, field=None):
		return sp.median(val_list(data, key_filter=field))

	