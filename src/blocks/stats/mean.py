from celery import task
from lib.block import Block
from lib.selector import val_list
import numpy as np

@task(base=Block)
class Mean(Block):

	meta = {
		'name': 'Mean',
		'desc': 'Discovers mean values in a field of a dataset',
		'connections': {
			'inputs': {
				'0': {
					'name': 'data',
					'desc': 'The source data set',
					'type': list
				}
			},
			'outputs': {
				'0': {
					'name': 'mean_val',
					'desc': 'The mean value of the data',
					'type': int
				}
			}
		},
		'params': {
			'field': {
				'type': str,
				'default': '',
				'desc': "The field of the data set to base the mean value off of"
			}
		}
   	}
	
	def run(self, data, field=None):
		return np.mean(val_list(data, key_filter=field))
