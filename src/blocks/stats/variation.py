from lib.block import Block
from lib.selector import val_list
from celery import task
from scipy.stats.mstats import variation


@task(base=Block)
class Variation(Block):
    
    meta = {
        'name': 'Variation',
        'desc': 'Calculates the amount of variation between values for the specified field(s)',
        'connections': {
            'inputs': {
                '0': {
                    'name': 'data',
                    'desc': 'Data to be acted upon',
                    'type': list
                },
                '1': {
                    'name': 'fields',
                    'desc': 'The fields to base the variation values off of',
                    'type': list
                },
                '2': {
                    'name': 'axis',
                    'desc': 'Axis along which to calculate the coefficient of variation',
                    'type': int
                }
            },
            'outputs': {
                '0': {
                    'name': 'total_val',
                    'desc': 'The variation of the dataset',
                    'type': float
                }
            }
        },
        'params': {
            'field': {
                'type': str,
                'default': '',
                'desc': "The field of the dataset to base outlier values off of"
            },
        }
    }
    
    def run(self, data, field=None, axis=None):
        if not isinstance(data, list):
            raise TypeError('Incoming data must be encapsulated by a list')
        if not isinstance(axis, int) and axis is not None:
            raise ValueError('Axis value must be an integer or None')
        
        return variation(val_list(data, key_filter=field))

