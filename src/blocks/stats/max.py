from celery import task
from lib.block import Block
from lib.selector import val_list
from scipy.stats import morestats as stats

@task(base=Block)
class Max(Block):

	meta = {
		'name': 'Max',
		'desc': 'Discovers maximum value in a field of a dataset',
		'connections': {
			'inputs': {
				'0': {
					'name': 'data',
					'desc': 'The source data set',
					'type': list
				}
			},
			'outputs': {
				'0': {
					'name': 'max_val',
					'desc': 'The max value of the data',
					'type': int
				}
			}
		},
		'params': {
			'field': {
				'type': str,
				'default': '',
				'desc': "The field of the data set to base the max value off of"
			}
		}
	}

	def run(self, data, field=None):
		return stats.amax(val_list(data, key_filter=field))
