from celery import task
from lib.block import Block
from lib.selector import val_list
import scipy as sp

@task(base=Block)
class stdDeviation(Block):

	meta = {
		'name': 'Standard Deviation',
		'desc': 'Discovers standard deviation values in a field of a dataset',
		'connections': {
			'inputs': {
				'0': {
					'name': 'data',
					'desc': 'The source data set',
					'type': list
				}
			},
			'outputs': {
				'0': {
					'name': 'std_deviation',
					'desc': 'The standard deviation value of the data',
					'type': int
				}
			}
		},
		'params': {
			'field': {
				'type': str,
				'default': '',
				'desc': "The field of the data set to base the max value off of"
			}
		}
   	}

	def run(self, data, field=None):
		return sp.std(val_list(data, key_filter=field))
