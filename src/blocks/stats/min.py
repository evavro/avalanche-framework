from celery import task
from lib.block import Block
from lib.selector import val_list
import numpy as np

@task(base=Block)
class Min(Block):

	meta = {
		'name': 'Min',
		'desc': 'Discovers minimum value in a field of a dataset',
		'connections': {
			'inputs': {
				'0': {
					'name': 'data',
					'desc': 'The source data set',
					'type': list
				}
			},
			'outputs': {
				'0': {
					'name': 'min_val',
					'desc': 'The min value of the data',
					'type': int
				}
			}
		},
		'params': {
			'field': {
				'type': str,
				'default': '',
				'desc': "The field of the data set to base the median value off of"
			}
		}
   	}

	def run(self, data, field=None):
		return np.min(val_list(data, key_filter=field))

	