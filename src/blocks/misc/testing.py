__author__ = 'Eric'

from lib.block import *
from celery import task

import numpy.random as nprd


@task(base=Block)
class RandomNumberSource(Block):

    meta = {
        'name': "Random Number Source",
        'desc': "Generates a configurable amount of random numbers",
        'connections': {
            'inputs': {},
            'outputs': {
                '0': {
                    'name': 'rand_out',
                    'desc': 'The generated random numbers'
                }
            }
        },
        'params': {
            'min': {
                'type': int,
                'default': 0,
                'desc': "The minimum random value (inclusive)"
            },
            'max': {
                'type': int,
                'default': 100,
                'desc': "The maximum random value (inclusive)"
            },
            'num': {
                'type': int,
                'default': 100,
                'desc': "The number of random numbers to be generated"
            }
        }
    }

    def run(self, *args, **params):
        low, high, num = params.get('min', 0), params.get('max', 100), params.get('num', 100)
        return nprd.random_integers(low, high, num)


@task(base=Block)
class Printer(Block):
    meta = {
        'name': 'Printer',
        'desc': 'Debug Block',
        'connections': {
            'inputs': {
                '0': {
                    'name': 'inputs',
                    'desc': 'Data to be printed',
                    'type': object
                }
            },
            "outputs": {}
        },
        "params": {}
    }

    def run(self, *args, **params):
        print("Runner Running")
        for k in params:
            print(params[k])
        for arg in args:
            print(arg)
        return list(args)


@task
class AddBlock(Block):
    
    loose_meta = True
    
    def run(self, x, y):
        return x + y


@task
class MultiplyBlock(Block):
    
    loose_meta = True

    def run(self, x, y):
        return x * y


@task(base=Block)
class MessageProxy(Block):

    meta = {
        'name': "Message Proxy",
        'desc': "Takes in a string and passes it onto another block",
        'connections': {
            'inputs': {
                '0': {
                    'name': 'message',
                    'desc': 'The message to carry'
                }
            },
            'outputs': {
                '0': {
                    'name': 'message',
                    'desc': 'The carried message'
                }
            }
        },
        'params': {
            'message': {
                'type': str,
                'default': "",
                'desc': "The message to carry"
            }
        }
    }

    def run(self, *args, **params):
        return self.flex_input(args, params, param_field='message')
        
@task(base=Block)
class StrictAddition(Block):
    
    meta = {
        'name': 'Strict Addition',
        'desc': 'Takes in ONLY integers and adds them together',
        'params': {
                'x': {
                    'desc': 'First number',
                    'type': int
                },
                'y': {
                    'desc': 'Second number',
                    'type': int
                }
        },
        'connections': {
            'inputs': { },
            'outputs': {
                '0': {
                    'name': 'sum',
                    'desc': 'The end result',
                    'type': int
                }
            }
        },
    }

    @strict_input
    def run(self, *args, **params):
        num1 = params['x']
        num2 = params['y']
        return num1 + num2
    
@task(base=Block)
class StoredAddition(Block):
    
    meta = {
        'name': 'Stored Addition',
        'desc': 'Addds together two integers and adds the result to the block history database',
        'params': {
                'x': {
                    'desc': 'First number',
                    'type': int
                },
                'y': {
                    'desc': 'Second number',
                    'type': int
                }
        },
        'connections': {
            'inputs': { },
            'outputs': {
                '0': {
                    'name': 'sum',
                    'desc': 'The end result',
                    'type': int
                }
            }
        },
    }

    @store_result
    def run(self, *args, **params):
        num1 = params['x']
        num2 = params['y']
        return num1 + num2

@task(base=Block)
class Getter(Block):
    meta = {
        'name': 'Getter',
        'desc': 'Debug Block',
        'connections': {
            'inputs': {
                '0': {
                    'name': 'inputs',
                    'desc': 'Object to call get() on',
                    'type': object
                }
            },
            "outputs": {}
        },
        "params": {}
    }

    def run(self, arg):
        print("\n\nRunner Running\n\n")
        val = arg.get()
        print(val)
        return val
    
@task(base=Block)
class ResultInput(Block):
    meta = {
        'name': 'Celery result input',
        'desc': 'Determines if any of the inputs contain an unexecuted celery task',
        'connections': {
            'inputs': {
                '0': {
                    'name': 'inputs',
                    'desc': 'Object to call get() on',
                }
            },
            "outputs": {}
        },
        "params": {}
    }
    
    loose_meta = True

    @safe_results
    def run(self, *args, **params):
        print "\n\nRunning Result Input block\n\n"
        for a in args:
            if isinstance(a, ResultBase):
                return True
        for p in params:
            if isinstance(params[p], ResultBase):
                return True
        return False
            