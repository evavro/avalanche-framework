from lib.block import Block, IllegalBlockException
from lib.selector import NOTHING
from celery import Task, task
import celery
from celery.result import ResultBase

import networkx as nx
import time



@task
class Scheduler(Task):
    def run(self, blocks, conn):
        dependency_graph = self.parse(conn) #connections dictionary

        working_graph = dependency_graph.copy() # graph we remove nodes from

        results = {}
        running = []
        for block in blocks:
            results[block] = None

        run_queue = self.group_sort(working_graph)[0]
        while len(run_queue) != 0:
            for b in run_queue:
                if b in running:
                    if not isinstance(results[b], ResultBase) or results[b].ready():
                        working_graph.remove_node(b)
                        running.remove(b)
                        #endif
                else:
                    if working_graph.in_degree(b) == 0:
                        if dependency_graph.in_degree(b) == 0:
                            results[b] = blocks[b]['class'].delay(**blocks[b]['params'])
                            running.append(b)
                        else:
                            # lookup the arguments and check if ready. If all arguments are ready,
                            params = blocks[b]['params']
                            pr = dependency_graph.predecessors(b)
                            args = [None]*len(pr)
                            for arg in pr:
                                argn = dependency_graph[arg][b]['inputs']
                                args[argn] = results[arg]

                            results[b] = blocks[b]['class'].delay(*args, **params)
                            running.append(b)


            # regen the head group. This effectively gets all nodes with degree 0 that we are waiting on

            run_queue = self.group_sort(working_graph)[0]

            time.sleep(0.1)# wait a bit

        return results

    @staticmethod
    def parse(con):
        wg = nx.DiGraph()
        for block in con:
            bc = con[block]
            for outs in bc['outputs']:
                outb_a = bc['outputs'][outs]
                for outb in outb_a:
                    if outb is not None:
                    #                        print(block, '[', outs, ']', '->', outb)
                        if not wg.has_edge(block, outb):
                            wg.add_edge(block, outb)
                        wg[block][outb]['outputs'] = outs
            for ins in bc['inputs']:
                inb_a = bc['inputs'][ins]
                for inb in inb_a:
                    if inb is not None:
                        if not wg.has_edge(inb, block):
                            wg.add_edge(inb, block)
                        wg[inb][block]['inputs'] = ins
        return wg

    @staticmethod
    def group_sort(_graph):
        graph = _graph.copy()
        res = []
        while graph.order() > 0:
            temp = []
            for n in graph.nodes():
                if graph.in_degree(n) == 0:
                    temp.append(n)
            graph.remove_nodes_from(temp)
            res.append(temp)
        res = [[]] if res == [] else res
        return res


@task
class Workflow(Task):
    def run(self, *args, **params):
        blocks = params['blocks']

        block_params = params['block_params']
        s_blocks = {}
        for b in blocks:
            s_blocks[b] = {}
            s_blocks[b]['class'] = Block.parse_name(blocks[b])
            s_blocks[b]['params'] = block_params[b]

        conn = params['connections']

        return Scheduler.run(s_blocks, conn)


# TODO Add workflow id
@task
class WorkflowOld(Task):
    # TODO - Support multiple connections
    def get_block_conn_in(self, bid):
        return self.connections_in[bid]['0']

    def get_block_conn_out(self, bid):
        return self.connections_out[bid]['0']

    def get_start_blocks(self):
        start_blocks = []
        for bid in self.blocks:
            if self.get_block_conn_in(bid) in NOTHING:
                start_blocks.append(bid)
        return start_blocks

    def unserialize_chain(self, chain):
        tasks = None
        for bid in chain:
            block = Block.parse_name(self.blocks[bid])
            params = self.block_params[bid]
            if tasks is None:
                tasks = [block.s(None, **params)]
            else:
                tasks.append(block.s(**params))
        return celery.chain(tasks)()

    def unserialize(self):
        chains, results = [[b] for b in self.get_start_blocks()], []
        for c in chains:
            start_block = c[0]
            next_block = self.get_block_conn_out(start_block) # FIXME support multiple outputs, ensure 'inputs' matches
            while next_block not in NOTHING:
                c.append(next_block)
                next_block = self.get_block_conn_out(next_block)
            results.append(self.unserialize_chain(c).get())
        return results

    # FIXME - remove self. and pass data functionally
    def run(self, *args, **params):
        self.blocks = params['blocks']
        self.block_params = params['block_params']
        self.connections_in = params['connections']['inputs']
        self.connections_out = params['connections']['outputs']

        return self.unserialize()


@task
class WorkflowParser(Task):
    def parse_blocks(self, blocks):
        wf_blocks = {}
        for bid in blocks:
            block = blocks[bid]
            clazz = block['class']
            if(Block.is_valid_name(clazz)):
                wf_blocks.update({bid: block['class']})
            else:
                raise IllegalBlockException(
                    'Could not parse block from workflow schema, illegal class name: %s' % (clazz, ))
        return wf_blocks

    # TODO - Validate params
    def parse_params(self, blocks):
        return dict([(bid, blocks[bid]['params']) for bid in blocks])

    def parse_connections(self, connections):
        connections_in = {}
        connections_out = {}
        for bid in connections:
            if bid not in NOTHING:
                connections_in.update({bid: connections[bid]['inputs']})
                connections_out.update({bid: connections[bid]['outputs']})
        return {"inputs": connections_in, "outputs": connections_out}

    def fmt_con(self, arg):
        arr = arg.copy()
        for a in arr:
            for b in arr[a]:
                keys = arr[a][b].keys()
                for k in keys:
                    if k.__class__ == str:
                        arr[a][b][int(k)] = arr[a][b][k]
                        arr[a][b].pop(k)
                for c in arr[a][b]:
                    if arr[a][b][c] is None:
                        arr[a][b][c] = []
                    elif arr[a][b][c].__class__ is not list:
                        arr[a][b][c] = [arr[a][b][c]]
        return arr

    # TODO - Validate before running
    def run(self, json_data):
        print "JSON DATA: %s" % (json_data)
        workflows = None
        for wf_data in json_data:
            sub_wf_data = json_data[wf_data]
            print "SUB WF: %s, %s" % (sub_wf_data, type(sub_wf_data))
            blocks = self.parse_blocks(sub_wf_data['blocks'])
            paramz = self.parse_params(sub_wf_data['blocks'])
            #            conns  = self.parse_connections(sub_wf_data['connections'])
            conns = self.fmt_con(sub_wf_data['connections'])
            if workflows is None:
                workflows = [Workflow.s(None, blocks=blocks, block_params=paramz, connections=conns)]
            else:
                workflows.append(Workflow.s(blocks=blocks, block_params=paramz, connections=conns))
        return celery.group(workflows)().get()

    # remove any database keys that will screw with the parsing process
    def clean(self, wf):
        raise NotImplementedError('TODO')

    @staticmethod
    def validate(wf):
        raise NotImplementedError('TODO')
    
