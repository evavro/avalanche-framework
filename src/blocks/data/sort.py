from celery import task
from lib.block import Block, ignore_meta
from lib.selector import val_list

@task(base=Block)
class Sort(Block):

    meta = {
        'name': 'Sort',
        'desc': 'Determines the unique elements in a set of data',
        'connections': {
            'inputs': {
                '0': {
                    'name': 'data',
                    'desc': 'The data set',
                    'type': list,
                    },
                },
            'outputs': {
                '0': {
                    'name': 'sorted',
                    'desc': 'The unique elements',
                    'type': list
                }
            }
        },
        'params': {
            'field': {
                'type': str,
                'default': '',
                'desc': 'The field of the dataset to base sorting on'
            },
        }
    }

    def run(self, data, field=None):
        return sorted(val_list(data, key_filter=field))
    