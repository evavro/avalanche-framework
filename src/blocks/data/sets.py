from celery import task
from lib.block import Block, ignore_meta


@task(base=Block)
class Intersection(Block):
    
    meta = {
        'name': 'Intersection',
        'desc': 'Determines the intersection set between two sets of data',
        'connections': {
            'inputs': {
                '0': {
                      'name': 'ds1',
                      'desc': 'First data set',
                      'type': list,
                },
                '1': {
                      'name': 'ds2',
                      'desc': 'Second data set',
                      'type': list,
                }
            },
            'outputs': {
                '0': {
                      'name': 'intersection',
                      'desc': 'The intersection set',
                      'type': list
                }
            }
        },
        'params': { }
    }
    
    def run(self, ds1, ds2):
        return list(set(ds1) & set(ds2))


@task(base=Block)
class Difference(Block):
    
    meta = {
        'name': 'Difference',
        'desc': 'Determines the difference between two sets of data',
        'connections': {
            'inputs': {
                '0': {
                      'name': 'ds1',
                      'desc': 'First data set',
                      'type': list,
                },
                '1': {
                      'name': 'ds2',
                      'desc': 'Second data set',
                      'type': list,
                }
            },
            'outputs': {
                '0': {
                      'name': 'diff',
                      'desc': 'The difference set',
                      'type': list
                }
            }
        },
        'params': { }
    }
    
    def run(self, ds1, ds2):
        return list(set(ds1) - set(ds2))
    
    
@ignore_meta
@task(base=Block)
class Union(Block):
    
    meta = {
        'name': 'Union',
        'desc': 'Determines the union set between two sets of data',
        'connections': {
            'inputs': {
                '0': {
                      'name': 'ds1',
                      'desc': 'First data set',
                      'type': list,
                },
                '1': {
                      'name': 'ds2',
                      'desc': 'Second data set',
                      'type': list,
                }
            },
            'outputs': {
                '0': {
                      'name': 'union',
                      'desc': 'The union set',
                      'type': list
                }
            }
        },
        'params': { }
    }
    
    def run(self, ds1, ds2):
        return list(set(ds1) | set(ds2))
    
    
@task(base=Block)
class Unique(Block):
    
    meta = {
        'name': 'Unique',
        'desc': 'Determines the unique elements in a set of data',
        'connections': {
            'inputs': {
                '0': {
                      'name': 'data',
                      'desc': 'The data set',
                      'type': list,
                },
            },
            'outputs': {
                '0': {
                      'name': 'unique',
                      'desc': 'The unique elements',
                      'type': list
                }
            }
        },
        'params': { }
    }
    
    def run(self, data):
        return list(set(data))
    
    