from lib.block import Block, store_result
from lib.db import MongoDB
from lib.selector import val_list
from celery import task


# TODO - Add timestamp
@task(base=Block)
class BasicResult(Block):
    
    meta = {
        'name': "Results",
        'desc': "Stores the inputed data set",
        'connections': {
            'inputs': {
                '0': {
                    'name': 'data',
                    'desc': 'The data to be stored',
                }
            },
            'outputs': {
                '0': {
                    'name': 'data',
                    'desc': 'The stored data',
                    'view': {
                        # specifies how the return value 
                    }
                }
            }
        },
        'params': {
            'dsname': {
                'type': str,
                'default': 'Dataset',
                'desc': 'Name of the dataset'
            }
        }
    }

    # TODO - Take in workflow id
    def run(self, dataset, **params):
        mongo = MongoDB()
        dsname = params.get('dsname', self.meta['params']['default'])
        for data in dataset:
            mongo.insert_result(self.db_name(), data, {'dsname': dsname})
        return dataset


@task(base=Block)
class GraphResult(Block):
    
    meta = {
        'name': "Graph Results",
        'desc': "Stores the inputed data set and returns graph data representing it",
        'connections': {
            'inputs': {
                '0': {
                    'name': 'data',
                    'desc': 'The data to be stored',
                    'type': dict
                }
            },
            'outputs': {}
        },
        'params': {
            'name': {
                'type': str,
                'default': 'dataset',
                'desc': 'Name of the dataset'
            },
            'field': {
                'type': str,
                'default': None,
                'desc': 'The field of the dataset to base graph points off of'
            },
            'series': {
                'type': dict,
                'default': {},
                'schema': {
                    'type': dict,
                    'label': 'Label',
                    'field': '_id',
                    'field_val': None
                },
                'desc': 'The schematics for the series'
            },
            'range': {
            }
        }
    }
            
    def run(self, dataset, series=None):
        labeled_series = []
        for s in series:
            label = s['label']
            field = s['field']
            expected_field_val = s['field_val']
            field_vals = val_list(dataset, key_filter=field)
            for fv in field_vals:
                if expected_field_val is None or fv == expected_field_val:
                    labeled_series.append({'name': label, 'data': fv})
                    #labeled_series.append({label: fv})
        return labeled_series
    
