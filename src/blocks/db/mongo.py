from lib.block import Block
from lib.db import MongoDB
from celery import task

# TODO - Update 'collection_name' to be a list of all of the valid collections
@task(base=Block)
class Mongo(Block):
    
    meta = {
        'name': "Database (Mongo)",
        'desc': "Returns a queryable collection from the MongoDB backend",
        'connections': {
            'inputs': {},
            'outputs': {
                '0': {
                    'name': 'mongo_out',
                    'desc': 'The desired collection'
                }
            }
        },
        'params': {
            'collection': {
                'type': str,
                'default': "",
                'desc': "The name of the collection"
            },
            'find': {
                'type': dict,
                'default': {},
                'desc': "Conditional field values"
            },
            'limit': {
                'type': int,
                'default': 0,
                'desc': "The number of results to return (0 for all)"
            },
            'sort': {
                'type': str,
                'default': "",
                'desc': "The field in which the collection's ordering will be based off of"
            }
        }
    }

    def run(self, *args, **params):
        return MongoDB().query(**params)

