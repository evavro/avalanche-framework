from celery import Celery
import celeryconfig

celery = Celery()
celery.config_from_object(celeryconfig)

'''
celery = Celery(broker="mongodb://localhost/avalanche_results", backend="mongodb://localhost/avalanche_results") 
'''
