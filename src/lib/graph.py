'''
Created on Dec 3, 2012

@author: dev
'''

class Graph(object):
    
    series = []
    
    def __init__(self):
        pass
    
    def add_series(self, label, dataset):
        self.series.append({label: dataset})
                
    def add_special_series(self, label, dataset, value_filter=None):
        for data in dataset:
            self.series.append({label: self.filter_by_val(data, value_filter)})
            
    def filter_by_val(self, data, val_filter=None):
        return filter(lambda val: val_filter is None or val == val_filter, data)
                
    def label_series(self, ):
        pass
    
class Highcharts(Graph):
    
    series = []
    
    def __init__(self):
        pass
    
    def add_series(self, label, data):
        pass
        