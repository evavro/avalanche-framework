from pymongo import Connection, ASCENDING, DESCENDING
from pymongo.collection import Collection

import datetime

class MongoDB(object):
    
    def __init__(self, connection=Connection('localhost', 27017), db='avalanche'):
        self.connection = connection
        self.db = self.connection[db]
        
    def query(self, **params):
        collection = Collection(self.db, params.get('collection', None))
        find = params.get('find', {})
        sort = params.get('sort', '_id')
        direct = DESCENDING if sort.endswith('-') else ASCENDING
        return collection.find(find).sort(sort, direct)
    
    def insert(self, collection_name, data, meta_data={}):
        if isinstance(meta_data, dict) and meta_data != {}:
            data.update(meta_data)
        data.update({'date': datetime.datetime.utcnow()})
        return Collection(self.db, collection_name).insert(data) 
    
    def remove(self, collection_name, query={}):
        Collection(self.db, collection_name).remove(query)
        
