import random
import numpy as np
from collections import Iterable

NOTHING = [None, 'None', '', ""]

# recursively flattens multidimensional lists into single dimensional ones
def flatten(l):
    for el in l:
        if isinstance(el, Iterable) and not isinstance(el, basestring):
            for sub_el in flatten(el):
                yield sub_el
        else:
            yield el
            
def np_flatten(l):
    return np.array(l).flatten()
            
# TODO use tail recursion so flatten doesn't need to be called later
def search_dict(d, field=None, val_type=None):
    found = []
    for k,v in d.items():
        if isinstance(v, dict):
            found.append(search_dict(v, field, val_type)) 
        else:
            if (field in NOTHING or k == field) and (val_type in NOTHING or type(v) == val_type):
                return [v]
    return [i for i in flatten(found)]

# recursively appends the values of fields in a dictionary to a list
# TODO support multiple fields. this is currently single variable based
def val_list(data, key_filter=None, type_filter=None, dig=True):
    if isinstance(data, dict):
        return search_dict(data, key_filter, type_filter)
    elif isinstance(data, list):
        l = []
        for x in data:
            if isinstance(x, list):
                l += [i for i in flatten(val_list(x, key_filter, type_filter, dig))]
            elif isinstance(x, dict):
                l += search_dict(x, key_filter, type_filter)
            else:
                if type_filter is None or type(x) == type_filter:
                    l.append(x)
        return l
    else:
        return data
    
# single variable numpy matrix
def sv_matrix(data, **params):
    data = val_list(data, **params)
    arr = []
    for i in data:
        if not isinstance(i, Iterable):
            arr.append([i])
        else:
            arr.append(i)
    return np.array(arr)

    
def shuffle(data):
    if isinstance(data, list):
        return random.shuffle(data)
    else:
        raise TypeError("Only a list can be shuffled")

