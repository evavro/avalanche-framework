blocks_package = 'blocks' # TODO - Load from framework config
blocks_result_db_col = 'BlockResult'

import re, ast
import pickle
from functools import wraps
from lib.db import MongoDB

from celery import Task
from celery.utils.log import get_task_logger
from celery.result import ResultBase

logger = get_task_logger(__name__)
db = MongoDB()


def import_blocks():
    exec('from %s import *' % (blocks_package,)) in globals()
    
def strict_input(run_method):
    @wraps(run_method)
    def fun(self, *args, **params):
        if self.meta is None or self.meta.get('params', None) is None:
            logger.warning('%s has no meta data concerning parameters - strict input validation cannot be applied' % (self,))
        else:
            if not self.loose_meta:
                self.is_valid_args(args)
                self.is_valid_params(params)
            else:
                logger.warning('Skipping input validation because the run method is decorated with @loose_input')
        return run_method(self, *args, **params)
    return fun

def loose_input(run_method):
    @wraps(run_method)
    def fun(self, *args, **params):
        self.loose_meta = True
        return run_method(self, *args, **params)
    return fun

def safe_results(run_method):
    @wraps(run_method)
    def fun(self, *args, **params):
        def res_filter(inputs):
            for (i, inn) in enumerate(inputs):
                val = inn if not isinstance(inputs, dict) else inputs[inn]
                if isinstance(val, ResultBase):
                    if isinstance(inputs, list):
                        inputs[i] = val.get()
                    elif isinstance(inputs, dict):
                        inputs[inputs.keys()[i]] = val.get()
            return inputs
        new_args = res_filter(list(args))
        new_params = res_filter(params)
        return run_method(self, *new_args, **new_params)                
    return fun

def result_history(block_db_name):
    hist_query = {'collection': blocks_result_db_col,
                  'find': {'block': str(block_db_name)}}
    res_hist = []
    for res in db.query(**hist_query):
        task_meta = db.query(collection='blocks_meta', find={'_id': res['task_id']})
        for i in task_meta:
            res_hist.append(pickle.loads(i['result']))
    return res_hist

# TODO - Set up a way so that meta data, like k-cluster nums, can correlate with data sets and be persisted (need to check from mongo?)
#    - Find BlockResult id from incoming celery result (task_id). Use BlockResult id in the meta data!

'''
The primary data structure of Avalanche
'''
class Block(Task):

    abstract, meta, loose_meta, ignore, ignore_meta = True, {}, False, False, False

    def __init__(self):
        pass
    
    @strict_input
    @safe_results
    def __call__(self, *args, **kwargs):
        logger.info('[Avalanche] - Calling task %r' % (self,))
        return self.run(*args, **kwargs)

    def run(self, *args, **kwargs):
        raise NotImplementedError('The run method must be implemented')

    def after_return(self, retval, task_id, *args, **kwargs):
        logger.info('Task returned: %r' % (self.request,))
        return retval

    def on_success(self, retval, task_id, args, kwargs):
        logger.info('Task #%s returned with the following: %s' % (task_id, retval,))
        return retval

    def on_failure(self, exc, task_id, args, kwargs, einfo):
        logger.info('An error occurred in task #%s during stage execution: %s\n%s' % (task_id, exc, einfo,))

    def on_retry(self, exc, task_id, args, kwargs, einfo):
        logger.info('An error occurred in task #%s during stage execution, attempting to retry: %s\n%s' % (task_id, exc, einfo,))
       
    def db_name(self):
        return self.__class__.__name__
    
    def task_id(self):
        return self.request.id
    
    def result_history(self):
        return result_history(self.db_name())
    
    # allows connections and parameters to provide data to a block in a flexible, prioritized manner (args > params)
    def flex_input(self, args, params, arg_i=0, param_field=None, both=False):
        self.is_valid_args(args)
        self.is_valid_params(params)
        selected_args = args[arg_i] if (len(args) > 0 and isinstance(arg_i, int)) else args
        selected_params = {}
        if params is not None and params != {}:
            selected_params = params.get(param_field, None) if param_field is not None else params
        else:
            return selected_args
        return selected_params if not both else (selected_args, selected_params)
    
    def is_valid_meta(self):
        meta = self.meta
        if not self.loose_meta or meta is None:
            if isinstance(meta, dict):
                if not (meta.has_key('name') and meta.has_key('desc') and meta.has_key('connections')):
                    raise AttributeError("%s: Block meta data must specify 'name', 'desc', and 'connections' keys" % (self,))
                conns = meta['connections']
                if not conns.has_key('inputs') and conns.has_key('outputs'):
                    raise AttributeError("%s: Block connection meta data must contain an 'inputs' and an 'outputs' key" % (self,))
            else:
                raise TypeError("%s: Block meta data must be a dictionary" % (self,))
            return True
    
    # TODO - support explicit (e.g., run(x, y))
    def is_valid_args(self, args):
        self.is_valid_meta()
        conns_in = self.meta['connections']['inputs']
        for arg in args:
            expected_type = conns_in.get('type')
            actual_type = type(arg)
            if expected_type is not None and expected_type is not actual_type:
                raise TypeError("Incoming block data is of an invalid type for argument '%s' (%s), expecting %s" % (arg,actual_type,expected_type,))
        return True
    
    def is_valid_params(self, params):
        self.is_valid_meta()
        supported = self.meta['params']
        for param in params:
            if not supported.has_key(param):
                raise KeyError("An unsupported parameter key was provided for this block: '%s'" % (param,))
            supported_param = supported.get(param)
            expected_type = supported.get(param)['type'] if supported_param is not None else None
            actual_type = type(params[param])
            if expected_type is not None and expected_type is not actual_type:
                raise TypeError("Block parameter '%s' from JSON has an invalid type (%s), expecting %s" % (param,actual_type,expected_type,))
        return True
    
    @staticmethod
    def is_valid_name(class_name):
        return Block.parse_name(class_name) is not None
        
    @staticmethod
    def all():
        import_blocks()
        classes, found = globals(), []
        for clas in classes:
            block = classes[clas]
            if isinstance(block, Block) and not block.ignore:
                found.append(clas)
        return found
    
    @staticmethod
    def all_meta():
        blocks = Block.all()
        all_meta = {'blocks': {}}
        for block in blocks:
            meta = globals()[block].meta
            meta_safe = ast.literal_eval(re.sub("<type '([^']+)'>", r"'\1'", str(meta)))
            if meta_safe is not None and meta_safe != {}:
                all_meta['blocks'].update({block: meta_safe})
        return all_meta
    
    @staticmethod
    def all_valid_meta():
        blocks = Block.all()
        for b in blocks:
            block = globals()[b]
            if not block.ignore_meta:
                block.is_valid_meta()
        return True
        
    @staticmethod
    def parse_name(class_name):
        import_blocks()
        if not isinstance(class_name, str) and not isinstance(class_name, unicode):
            raise IllegalBlockException('Block names must be a string or unicode: %s' % (class_name,))
        clas = globals()[class_name]
        if clas is None:
            raise IllegalBlockException('Unrecognized block class name: %s' % (class_name,))
        return clas
    
'''
Decorators
'''

def input_historical(run_method):
    @wraps(run_method)
    def fun(self, *args, **params):
        args[0] = self.result_history()
        return run_method(self, *args, **params)
    return fun

# An alternative access point to the task results stored by celery.
# Allows for collections of historical input, since celery only stores runtime ids, not task info
def store_result(run_method):
    @wraps(run_method)
    def fun(self, *args, **params):
        db.insert(blocks_result_db_col, {'block': self.db_name(), 'task_id': self.task_id()})
        return run_method(self, *args, **params)
    return fun

def ignore(clas):
    clas.ignore = True
    return clas

def ignore_meta(clas):
    clas.ignore_meta = True
    return clas

'''
Exceptions
'''
class IllegalBlockException(Exception):

    def __init__(self, message):
        self.message = message
        
