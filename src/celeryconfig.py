__author__ = 'Erik'


BROKER_URL = 'mongodb://127.0.0.1:27017/avalanche'

# avalanche
INSTALLED_APPS = ('avalanche')

# List of modules to import when celery starts.
CELERY_IMPORTS = ('lib', 'blocks')

# Stage results (meta deta) db
CELERY_RESULT_BACKEND = 'mongodb'
CELERY_MONGODB_BACKEND_SETTINGS = {
    'host': '127.0.0.1',
    'port': 27017,
    'database': 'avalanche',
    'taskmeta_collection': 'blocks_meta',
}
CELERY_RESULT_ENGINE_OPTIONS = {"echo": True}

# Task serialization
#CELERY_TASK_SERIALIZER = 'json'
#CELERY_RESULT_SERIALIZER = 'json'
CELERY_TIMEZONE = 'UTC'
CELERY_ENABLE_UTC = True

