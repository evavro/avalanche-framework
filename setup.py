#!/usr/bin/python
# -*- coding: utf-8 -*-

from distutils.core import setup

setup(
    name=u'avalanche'.encode('utf-8'),
    version='0.1',
    author=u'Team Avalanche'.encode('utf-8'),
    author_email=u'avalanche@evavro.com'.encode('utf-8'),
    #packages=['', ],
    #package_dir={{'': 'src'}},
    #package_data={{'{package_name}': []}},
    #requires=['distribute', ],
    #    test_suite='nose.collector',
    setup_requires=['nose', 'Celery', 'celery-with-mongodb', 'pytz', 'numpy'],
    
    # Dependencies for the package.
    #install_requires=open('requirements.txt').readlines(),
    scripts=[],                     # List of python script files.
    #data_files=[('/etc/init.d', ['init-script'])]
    #url='',                        # Home page.
    #download_url='',               # Download url
    description=u'A data mining aggregator',
    long_description=open('README.rst').read(),
    # Full list of classifiers could be found at:
    # http://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        'License :: OSI Approved :: GNU General Public License (GPL)',
    ],
)
