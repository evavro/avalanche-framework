import os
import json
import ast
from flask import Flask, Response, request, redirect, json, jsonify, abort, make_response
from functools import wraps, update_wrapper
from urlparse import urlparse
from pymongo import Connection
from bson.objectid import ObjectId
import gridfs

import sys
sys.path.append("./src/")
from lib.block import Block
from blocks.workflow import *

MONGO_URL = os.environ.get('MONGOHQ_URL')

connection, db = None, None

if MONGO_URL:
    connection = Connection(MONGO_URL)
    db = connection[urlparse(MONGO_URL).path[1:]]
else:
    connection = Connection('localhost', 27017)
    db = connection['avalanche']

app = Flask(__name__)
app.config.from_pyfile('config.py')


def nocache(f):
    def new_func(*args, **kwargs):
        resp = make_response(f(*args, **kwargs))
        resp.cache_control.no_cache = True
        return resp
    return update_wrapper(new_func, f)


def check_login(username, password):
    return ({"username": "avalanche", "password": "aquatestla123"})
    #return db.users.find_one({ "username": username, "password": password })


def authenticate():
    return Response(
        'Could not verify your access level for the requested URL.\n'
        'You have to login with proper credentials', 401,
        {'WWW-Authenticate': 'Basic realm="Login Required"'})


def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if not auth or not check_login(auth.username, auth.password):
            return authenticate()
        return f(*args, **kwargs)
    return decorated


# TODO take in optional id, don't persist workflow if it is set
# TODO validate workflow schema

# FIXME - The class name isn't being saved in mongo
@app.route('/workflow/in/<run>', methods=['POST'])
def workflow_in(run):
    content = json.loads(request.data)
    if isinstance(content, dict):
        # TODO store result id so the correlation between workflow isn't temporary
        wfid = db.workflows.insert(content)
        if wfid is not None:
            app.logger.info('Successfully added workflow #%s to the pool' % (wfid,))
            if run == 'true':
                res = run_workflow_only_ids(wfid)
                return 'http://%s:8000/requests/api/v1/request/%s?format=json' % (request.remote_addr, res['result_id'])
            return json.dumps({'workflow_id': str(wfid)})
        else:
            abort('Failed to add workflow to the database for an unknown reason', 500)
    else:
        abort('Incoming data must be JSON compliant, is %s' % (content_type), 406)


def get_clean_workflow_json(wfid, strip_id=True):
    wf = db.workflows.find_one({'_id': ObjectId(wfid)})
    if strip_id and '_id' in wf:
        del wf['_id']
    return ast.literal_eval(json.dumps(wf))


@app.route('/workflow/run/<wfid>')
def rest_run_workflow(wfid):
    try:
        return json.dumps(run_workflow(wfid))
    except Exception as e:
        return "Workflow execution error: %s" % (e,)
    
    
def run_workflow(wfid):
    return {'result': WorkflowParser.delay(get_clean_workflow_json(wfid)).get()}
    
    
@app.route('/workflow/run/id/<wfid>')
def rest_run_workflow_only_ids(wfid):
    try:
        return json.dumps(run_workflow_only_ids(wfid))
    except Exception as e:
        return "Workflow execution error: %s" % (e,)


def run_workflow_only_ids(wfid):
    return {'result_id': str(WorkflowParser.delay(get_clean_workflow_json(wfid)))}


@app.route('/workflow/out/<wfid>')
def get_workflow_json(wfid):
    return json.dumps(get_clean_workflow_json(wfid))


@app.route('/workflow/new_id')
def generate_workflow_id():
    encoding, found = 'hex', False
    def rand_id():
        return os.urandom(16).encode(encoding)
    wfid = rand_id()
    while not found:
        if(db.workflows.find_one({'id': wfid}) is not None):
            wfid = rand_id()
        else:
            found = True
    return wfid


@app.route('/block_list')
@nocache
def block_list():
    return jsonify(**Block.all_meta())


@app.route('/data/in/<format>')
def data_in(format):
    # save to gridFS
    return 'Not implemented yet'


@app.route('/data/out/<id>')
def data_out(id):
    return 'Not implemented yet'


# implement error handler (page)
@app.route('/')
def index():
    try:
        Block.all_valid_meta()
    except Exception as e:
        return "Failed to initialize framework:<p>%s</p>" % (e,)
    return 'Avalanche Cloud Framework - RESTful API'


if __name__ == '__main__':
    app.logger.info("Initializing Avalanche Framework ...")
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port, debug=True)

