===========================================
Avalanche: Easy Data Aggregation
===========================================
        
**Avalanche Framework** is a generic data mining application.

Structure
-------

+   ``bin`` – directory, where scripts are placed.
+   ``src`` – directory, where package code is placed.
+   ``docs`` – directory, where package documentation is placed.
+   ``.travis.yml`` - travis build server
+   ``LICENCE.txt`` – LGPL license text.
+   ``Makefile``
+   ``README.rst``
+   ``requirements.txt`` - pip module requirements
+   ``setup.cfg`` – `setup configuration file 
    <http://docs.python.org/distutils/configfile.html>`_.
+   ``setup.py`` – `description
    <http://docs.python.org/distutils/introduction.html>`_.


Tools
-------

+   *sphinx* and *rst2pdf* – for documentation.
+   *nose* – for testing.
+   *coverage* - for code coverage.


How to start
-------

#.  Clone project:
	
	``git clone https://hur1can3@bitbucket.org/avalanche/avalanche-framework.git``

#.  Install Requirements:

    ``make init``

#.  Run Tests

    ``make test``


OS Requirements
-------

In Ubuntu you can install everything with command:
	
	``sudo apt-get install build-essential``

License
-------

Avalanche is licensed under a GPL license. See the LICENSE.txt file in the 
distribution.


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
